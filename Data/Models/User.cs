using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Conum.Data.Models
{
    public class User: IdentityUser
    {
        public IEnumerable<Counter> Counters { get; set; }

    }
}