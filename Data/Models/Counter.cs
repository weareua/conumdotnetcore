using System;
using System.Collections.Generic;

namespace Conum.Data.Models
{
    public class Counter
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Dimension { get; set; }

        public string Color { get; set; }

        public DateTime CreationDate { get;set; }

        public decimal Price { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public IEnumerable<Record> Records { get; set; }
    }
}