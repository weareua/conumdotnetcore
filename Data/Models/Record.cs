using System;
using System.Collections.Generic;

namespace Conum.Data.Models
{
    public class Record
    {
        public long Id { get; set; }
        public decimal Value { get; set; }
        public DateTime CreationDate { get; set; }
        public long CounterId { get; set; }
        public Counter Counter { get; set; }
    }
}