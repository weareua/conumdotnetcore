using System.Collections.Generic;
using Conum.Data.Models;

namespace Conum.Services
{
    public interface ICounterService
    {
        IEnumerable<Counter> GetAllForUser(string userId);

        Counter GetById(long id);

        void Create(Counter item);

        void Update(Counter item);

        void Delete(Counter item);
    }
}