using System.Collections.Generic;
using Conum.Data.Models;

namespace Conum.Services
{
    public interface IUserService
    {
        IEnumerable<User> GetAll();

        User GetById(string id);

        User GetByEmail(string email);
        void Create(User item);

        void Update(User item);

        void Delete(User item);
    }
}