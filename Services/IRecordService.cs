using System.Collections.Generic;
using Conum.Data.Models;

namespace Conum.Services
{

    public interface IRecordService
    {

        IEnumerable<Record> GetAllForCounter(long counterId);

        Record GetById(long id);

        void Create(Record item);

        void Delete(Record item);
    }
}