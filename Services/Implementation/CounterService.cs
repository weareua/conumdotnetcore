using System.Collections.Generic;
using Conum.Data.Models;
using Conum.Data.Context;
using Conum.Services;
using System.Linq;

namespace Conum.Services.Implementation
{

    public class CounterService: ICounterService
    {

        private readonly ConumContext _context;

        public CounterService(ConumContext context)
        {
            _context = context;
        }

        public IEnumerable<Counter> GetAllForUser(string userId)
        {
            if ( _context.Counters.Any(c => c.UserId == userId))
            {
                return _context.Counters.Where(c => c.UserId == userId).ToList();
            }
            return null;
        }

        public Counter GetById(long id)
        {
            var counter = _context.Counters.FirstOrDefault(t => t.Id == id);
            if (counter != null)
            {
                return counter;
            }
            else
            {
                return null;
            }
        }
        public void Create(Counter counter)
        {
            _context.Counters.Add(counter);
            _context.SaveChanges();
        }

        public void Update(Counter counter)
        {
            _context.Counters.Update(counter);
            _context.SaveChanges();
        }

        public void Delete(Counter counter)
        {
            _context.Counters.Remove(counter);
            _context.SaveChanges();
        }
    }
}