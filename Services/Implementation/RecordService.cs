using System.Collections.Generic;
using System.Linq;
using Conum.Data.Models;
using Conum.Data.Context;

namespace Conum.Services{

    public class RecordService: IRecordService
    {

        private readonly ConumContext _context;

        public RecordService(ConumContext context)
        {
            _context = context;
        }

        public IEnumerable<Record> GetAllForCounter(long counterId)
        {
            return _context.Records.Where(r => r.CounterId == counterId).OrderByDescending(
                r => r.CreationDate).ThenByDescending(r => r.Value).ToList();
        }
        public Record GetById(long id)
        {
            var record = _context.Records.FirstOrDefault(t => t.Id == id);
            if (record != null)
            {
                return record;
            }
            else
            {
                return null;
            }
        }
        public void Create(Record record)
        {
            _context.Records.Add(record);
            _context.SaveChanges();
        }
        public void Delete(Record record)
        {
            _context.Records.Remove(record);
            _context.SaveChanges();
        }
    }
}