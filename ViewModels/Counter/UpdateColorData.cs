using System.ComponentModel.DataAnnotations;

namespace Conum.ViewModels.Counter
{
    public class UpdateColorData
    {
        [Required]
        public long CounterId { get; set; }
        
        [Required]
        public string Color { get; set; }
    }
}