using System.ComponentModel.DataAnnotations;

namespace Conum.ViewModels.Counter
{
    public class DeleteCounterData
    {
        [Required]
        public long CounterId { get; set; }
    }
}