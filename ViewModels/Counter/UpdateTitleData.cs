using System.ComponentModel.DataAnnotations;

namespace Conum.ViewModels.Counter
{
    public class UpdateTitleData
    {
        [Required]
        public long CounterId { get; set; }
        
        [Required]
        public string Title { get; set; }
    }
}