using System.ComponentModel.DataAnnotations;

namespace Conum.ViewModels.Counter
{
    public class UpdateDimensionData
    {
        [Required]
        public long CounterId { get; set; }
        
        [Required]
        public string Dimension { get; set; }
    }
}