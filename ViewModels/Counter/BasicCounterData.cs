using System.ComponentModel.DataAnnotations;

namespace Conum.ViewModels.Counter
{
    public class BasicCounterData
    {
        [Required]
        public string UserId { get; set; }
        
        [Required]
        public string Title { get; set; }
        
        [Required]
        public string Dimension { get; set; }
        [Required]
        public string Color { get; set; }

        [Required]
        public decimal Price { get; set; }
    }
}