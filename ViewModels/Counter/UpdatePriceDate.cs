using System.ComponentModel.DataAnnotations;

namespace Conum.ViewModels.Counter
{
    public class UpdatePriceData
    {
        [Required]
        public long CounterId { get; set; }
        
        [Required]
        public decimal Price { get; set; }
    }
}