using System.ComponentModel.DataAnnotations;

namespace Conum.ViewModels.Record
{
    public class DeleteRecordData
    {
        [Required]
        public long RecordId { get; set; }
    }
}