using System.ComponentModel.DataAnnotations;

namespace Conum.ViewModels.Record
{
    public class AddRecordData
    {
        [Required]
        public int CounterId { get; set; }
        
        [Required]
        public decimal Value { get; set; }

        [Required]
        public string CreationDate { get; set; }
    }
}