﻿$(function () {
    // when the modal is closed
    $('#add-counter-modal-container').on('hidden.bs.modal', function () {
        // remove the bs.modal data attribute from it
        $(this).removeData('bs.modal');
        // and empty the modal-content element
        $('#add-counter-modal-container .modal-content').empty();
    });
});

$('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    language: "uk",
    autoclose: true
});

// Switch off forms autocomplete
$(document).ready(function(){
    $( document ).on( 'focus', ':input', function(){
        $( this ).attr( 'autocomplete', 'off' );
    });
});

// Records Diagram
$(document).ready(function(){
    var countersIdList = []
    $('.records-nest').each(function(index, item){
        countersIdList.push(item.dataset.counter);
    });

    $.each(countersIdList, function (index, item){
        var counterId = item;
        if ( $('#records-list-for-' + counterId).find('table tbody tr').length > 3 ) {
        
            var ctx = document.getElementById("records-chart-for-"+counterId).getContext('2d');
            
            var dirtyValues = $(".records-table-for-" + counterId + " .record-spent").slice(0,25)
            var dirtyCreationDates = $(".records-table-for-" + counterId + " .record-creation-date").slice(0,25)
            
            var counterColor = $('#show-button-for-' + counterId).css("background-color");

            var values = [];
            var creationDates = [];
            var backgroundColor = [];

            for (i=1; i<26; i++)
            {
                backgroundColor.push(counterColor);
            }

            dirtyValues.each(function() {
                values.push(this.innerText);
            });

            dirtyCreationDates.each(function() {
                creationDates.push(this.innerText);
            });

            // Reverse order for comfortable view
            var reversedValues = values.reverse();
            var reversedDates = creationDates.reverse();

            // Remove first value&date if it's 0
            if (reversedValues[0] === "0"){
                reversedValues.splice(0,1);
                reversedDates.splice(0,1);
            }

            var RecordsChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: reversedDates,
                    datasets: [{
                        label: 'Спожито за вказаний період часу',
                        backgroundColor: backgroundColor,
                        data: reversedValues,
                        borderWidth: 1
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        }
    });
});

// Colorpicker & buttons color for Edit section
$( ".edit" ).click(function() {
    var counterId = $(this).data('counter');
    $('.edit-color-for-' + counterId).colorpicker({
        format: 'hex'
    });
    
    var counterColor = $(this).css("background-color");
    var buttons = $('#edit-for-' + counterId + ' .btn-primary');
    buttons.css('background-color', counterColor);
    buttons.css('border-color', counterColor);

});

// Color for Add Record section
$( ".all-records" ).click(function() {
    var root = $(this).parent().parent().parent();
    var counterId = root.find(".records-nest").data('counter');

    // Set appropriate color
    var counterColor = $(this).css("background-color");
    var buttons = $('#add-new-record-for-' + counterId + ' .btn-primary');
    buttons.css('background-color', counterColor);
    buttons.css('border-color', counterColor);

});

// Remove edit button for mobiles
if ($(window).width() < 768) {
    $(".edit").hide();
    $(".device-alert").show();
    }
    else{
        $(".edit").show();
        $(".device-alert").hide();
    }
$(window).resize(function(){
   if ($(window).width() < 768) {
    $(".edit").hide();
    $(".device-alert").show();
    }
    else{
        $(".edit").show();
        $(".device-alert").hide();
    }
}) 