using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Conum.Services;
using Conum.ViewModels.Record;
using Conum.Data.Models;

namespace Conum.Controllers
{
    public class RecordController : Controller
    {
        private IRecordService _service;

        public RecordController(IRecordService service)
        {
            _service = service;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddRecord([FromForm] AddRecordData inputData)
        {
            if (ModelState.IsValid)
            {  
                var record = new Record{};
                record.Value = inputData.Value;
                record.CreationDate = DateTime.ParseExact(inputData.CreationDate, "d/M/yyyy", CultureInfo.InvariantCulture);
                record.CounterId = inputData.CounterId;

                _service.Create(record);
                
                TempData["Success"] = "Новий показник було успішно додано";
            }
            else
            {
                TempData["Error"] = true;
            }

            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteRecord([FromForm] DeleteRecordData inputData)
        {
            if (ModelState.IsValid)
            {  
                var record = _service.GetById(inputData.RecordId);

                _service.Delete(record);
                
                TempData["Success"] = "Обраний показник було успішно видалено";
            }
            else
            {
                TempData["Error"] = true;
            }

            return RedirectToAction("Index", "Dashboard");
        }
    }
}
