﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Conum.Services;
using Conum.ViewModels;
using Conum.Data.Models;

namespace Conum.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private ICounterService _counterService;
        private IRecordService _recordService;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public User user;

        public DashboardController(
            ICounterService counterService,
            IRecordService recordService,
            SignInManager<User> signInManager,
            UserManager<User> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _counterService = counterService;
            _recordService = recordService;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            user = _userManager.GetUserAsync(HttpContext.User).Result;
            if (user != null)
            {
                var userId = user.Id.ToString();
                user.Counters = _counterService.GetAllForUser(userId);
                if (user.Counters != null)
                {
                    Dictionary<long, decimal> recordsDifference = new Dictionary<long, decimal>();
                    
                    foreach (var counter in user.Counters)
                    {
                        counter.Records = _recordService.GetAllForCounter(counter.Id);
                        foreach (var record in counter.Records)
                        {
                            Record previousRecord = null;
                            decimal difference = 0;
                            
                            var recordsList = counter.Records.ToList();
                            for (int i = 0; i < recordsList.Count; i++)
                            {
                                if (recordsList[i].Id == record.Id)
                                {
                                    try
                                    {
                                        // Consider the desscending sort
                                        previousRecord = recordsList[i+1];
                                        difference = record.Value - previousRecord.Value;
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                            recordsDifference.Add(record.Id, difference);
                        }
                    } 
                    ViewBag.RecordsDifference = recordsDifference;
                }
            }

            ViewBag.User = user;

            return View();
        }

        public IActionResult ModalAddCounterAction()
        {
            ViewBag.UserId = _userManager.GetUserId(HttpContext.User);
            ViewBag.Email = _userManager.GetUserName(HttpContext.User);
            return PartialView("ModalAddCounter");
        }

        public IActionResult ModalDeleteRecordAction(long Id)
        {
            var record = _recordService.GetById(Id);
            ViewBag.Record = record;
            ViewBag.Counter = _counterService.GetById(record.CounterId);
            return PartialView("ModalDeleteRecord");
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}