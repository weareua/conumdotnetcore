using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Conum.Services;
using Conum.ViewModels.Counter;
using Conum.Data.Models;

namespace Conum.Controllers
{
    public class CounterController : Controller
    {
        private ICounterService _service;

        public CounterController(ICounterService service)
        {
            _service = service;    
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddCounter([FromForm] BasicCounterData inputData)
        {
            if (ModelState.IsValid)
            {  
                var counter = new Counter{};
                counter.Title = inputData.Title;
                counter.Dimension = inputData.Dimension;
                counter.Color = inputData.Color;
                counter.Price = inputData.Price;
                counter.CreationDate = DateTime.Now;
                counter.UserId = inputData.UserId;

                _service.Create(counter);

                TempData["Success"] = $"Лічильник \"{counter.Title}\" було успішно додано";
            }
            else
            {
                TempData["Error"] = true;
            }

            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateTitle([FromForm] UpdateTitleData inputData)
        {
            if (ModelState.IsValid)
            {   
                var counter = _service.GetById(inputData.CounterId);
                var oldTitle = counter.Title;
                counter.Title = inputData.Title;

                _service.Update(counter);

                TempData["Success"] = $"Назву лічильника \"{oldTitle}\" було успішно змінено";
            }
            else
            {
                TempData["Error"] = true;
            }
            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateDimension([FromForm] UpdateDimensionData inputData)
        {
            if (ModelState.IsValid)
            {   
                var counter = _service.GetById(inputData.CounterId);
                counter.Dimension = inputData.Dimension;

                _service.Update(counter);
                
                TempData["Success"] = $"Одиницю вимірювання лічильника \"{counter.Title}\" було успішно змінено";
            }
            else
            {
                TempData["Error"] = true;
            }
            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateColor([FromForm] UpdateColorData inputData)
        {
            if (ModelState.IsValid)
            {  
                var counter = _service.GetById(inputData.CounterId);
                counter.Color = inputData.Color;

                _service.Update(counter);
                
                TempData["Success"] = $"Колір лічильника \"{counter.Title}\" було успішно змінено";
            }
            else
            {
                TempData["Error"] = true;
            }

            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdatePrice([FromForm] UpdatePriceData inputData)
        {
            if (ModelState.IsValid)
            {  
                var counter = _service.GetById(inputData.CounterId);
                counter.Price = inputData.Price;

                _service.Update(counter);
                 
                TempData["Success"] = $"Вартість спожитої одиниці для \"{counter.Title}\" було успішно змінено";
            }
            else
            {
                TempData["Error"] = true;
            }

            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete([FromForm] DeleteCounterData inputData)
        {
            if (ModelState.IsValid)
            {  
                var counter = _service.GetById(inputData.CounterId);

                _service.Delete(counter);
                
                TempData["Success"] = $"Лічильник \"{counter.Title}\" було успішно видалено";
            }
            else
            {
                TempData["Error"] = true;
            }

            return RedirectToAction("Index", "Dashboard");
        }
    }
}
